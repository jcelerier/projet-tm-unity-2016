﻿using UnityEngine;
using System;

public class synth : MonoBehaviour
{
	public double gain = 0.5;
	private double phase = 0;
	private int pos = 0;

	bool collided = false;
	bool collided_decay = false;

	bool fmod_dir = false;
	private float freq_mod = 0;
	private float freq_pos = 0;
	int x= 0;
	void OnAudioFilterRead(float[] data, int channels)
	{
		for (int i = 0; i < data.Length; i += channels)
		{
			float freq = 400 + freq_mod;
			float cst = (float)(freq * 2.0 * Math.PI / 44100.0);
			float k = (float)(pos * cst);
			float val = (float)(gain * Math.Sin(k));

			data[i] = val;
			data[i + 1] = val;

			pos++;
			adsr ();
			fmod ();
		}
	}

	void adsr()
	{
		// Some kind of ADSR
		if (collided && gain < 0.3) {
			gain = gain + 0.001;
		} else if (collided && gain >= 0.3) {
			collided = false;
			collided_decay = true;
		} else if (collided_decay) {
			if (gain > 0.05)
				gain = gain - 0.00001;
			else
				collided_decay = false;
		}
	}

	void fmod()
	{
		if (freq_mod < -10f) {
			fmod_dir = true;
		} else if(freq_mod > 10f) {
			fmod_dir = false;
		}

		if (fmod_dir) {
			freq_mod += 0.0001f * freq_pos;
		} else {
			freq_mod -= 0.0001f * freq_pos;
		}
	}

	void OnCollisionEnter (Collision col)
	{
		collided = true;
		collided_decay = false;
	}
	void Update()
	{
		freq_pos = (float)transform.position.magnitude;
		//gain += freq / 5000000;
	}
} 